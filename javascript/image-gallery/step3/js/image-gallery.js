
/*
Note that the parameter defined in the function below means that it should be
an object that has two properties: el and images. Inside the body of the function
you can use el and images. This is an example of 'destructuring' (it's modern js)
*/

const createGallery = ({el, images}) => {
	console.log("The element that will contain")

	const template = ` 
	<img id="mainImg" src="" />
    <h3 id="caption"></h3>
    <input type="button" id="btnPrev" value="Prev" />
    &nbsp;
    <input type="button" id="btnNext" value="Next" />`;

	el.innerHTML = template;

	const mainImg = el.querySelector("#mainImg");
	const caption = el.querySelector("#caption");
	const btnPrev = el.querySelector("#btnPrev");
	const btnNext = el.querySelector("#btnNext");
	
	let currentImg = 0;
  
	// STEP 2
	// Create a function to display an image object
	const showImage = (imgObj) => {
	  mainImg.src = imgObj.path;
	  caption.innerHTML = imgObj.description;
	}
  
	showImage(images[currentImg]);
  
	// STEP 3
	// add an event handler function to the 'next' button
	btnNext.addEventListener("click", () => {
	  if(currentImg === 2){
		btnNext.disabled = true;
	  }else if(currentImg < 2){
		btnNext.removeAttribute('disabled');
		currentImg++;
		showImage(images[currentImg]);
		console.log(currentImg);
		
	  }
	  
	});
  
	// STEP 4
	// add an event handler function to the 'prev' button
	btnPrev.addEventListener("click", () => {
		if(currentImg === 0){
		btnPrev.disabled = true;
	  }else if(currentImg > 0){
		btnNext.removeAttribute('disabled');
		currentImg--;
		showImage(images[currentImg]);
		console.log(currentImg);
		
	  }
	  
	});
}