<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Week 1 Coding Problems</title>
	<script type="text/javascript">
		
		// JavaScript problems

		// Problem 1
		// Use let to declare a variable named 'book'.
		// Initialize the variable to an object that has a 'title' property and an 'author' property
		// (you can choose the values for each property)
		// Then console.log the book variable

		let book = new Object();

		book.title = 'Learn to Code';
		book.author = 'John Smith';

		console.log(book);


		// Problem 2
		// Use const to create an array of objects.
		// Each object represents a book that has a 'title' and 'author' property.
		// You can assign the values to each property (choose whatever values you like)
		// The array should have 3 objects in it

		const books = [
			{title:"Learn to Code", author:"John Smith"},
			{title:"Coding for Dummies", author:"Jane Doe"},
			{title:"Coding is Fun", author:"Aaron Tub"}
		];

		// Problem 3 
		// Use two different types of loops to iterate through the array that you created for problem 2. 
		// You can simply console log the title property of each book. 	

		
		console.log("foreach");
		books.forEach((ce) => {
            console.log(ce["title"]);
        });
        
		console.log("for");
        for(var x = 0; x < books.length; x++){
            console.log(books[x].title);
        }

		// Problem 4
		// Write an arrow function named addTwoNumbers.
		// The function should have two parameters (which should be numbers).
		// The function should add the two parameters and return the sum.
		// After you write the function, invoke it and console log the return value 
		// (you can pick the values for the parameters when you invoke the function)

		const addTwoNumbers = (n1,n2) => {
			return n1 + n2;
		}

		let add = addTwoNumbers(1,2);
		console.log(add);
		
		

	</script>
</head>
<body>
<?php
// PHP Problems

// Problem 5
// Create an array of associative arrays. Each associative array should represent 
// a book that has a title key, and an author key (you can choose the values for each key). 
// Put at least 3 books in the array.
	$books = array();
	
	$books = [
		['title' => "PHP for Dummies", 'author' => "Adam Smith"],
		['title' => "PHP is Fun", 'author' => "Jacob Barker"],
		['title' => "PHP Made Simple", 'author' => "Jane Brown"]
	];

// Problem 6
// Use two different types of loops to iterate through the array. 
// In each loop, simply echo the title of each book.
	foreach ($books as $b){
		$title = $b['title'] . "</br>";
		echo($title);
	}

	
	for($x = 0; $x < count($books); $x++){
		echo($books[$x]['title'] . "</br>");
	}


?>
</body>
</html>