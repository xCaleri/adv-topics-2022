import { createRouter, createWebHistory } from 'vue-router'
import HomeView from "./views/HomeView.vue";
// import BlogPost1 from "./views/BlogPost1.vue";
// import BlogPost2 from "./views/BlogPost2.vue";
import NotFound from "./views/NotFound.vue";
import BlogPost from "./views/BlogPost.vue";
import BlogList from "./views/BlogList.vue";
import BookList from "./views/BookList.vue";
import BookDetails from "./views/BookDetails.vue";

const routes = [
    { path: "/", component: HomeView, name: "home" },
    // { path: "/blog-post-1", component: BlogPost1, name: "post1" },
    // { path: "/blog-post-2", component: BlogPost2, name: "post2" },
    { path: '/:catchAll(.*)', component: NotFound },
    { path: "/blog/:id", component: BlogPost, name: "blog", props: true	},
    { path: "/blog/", component: BlogList, name: "blog-list" },
    { path:"/books/", component:BookList,name:"book-list" },
    { path: "/books/:id", component: BookDetails, name: "book-details", props:true },
];

const router = createRouter({
   history: createWebHistory(process.env.BASE_URL),
   routes
})
  
export default router