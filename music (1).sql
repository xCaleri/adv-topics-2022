-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 27, 2022 at 07:06 PM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE `album` (
  `album_id` int NOT NULL,
  `artist_id` int NOT NULL,
  `album_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`album_id`, `artist_id`, `album_name`, `year`) VALUES
(1, 1, 'Ugly is Beautiful', 2020),
(2, 2, 'BALLADS 1', 2018);

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE `artist` (
  `artist_id` int NOT NULL,
  `artist_name` varchar(50) NOT NULL,
  `record_label_id` int DEFAULT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`artist_id`, `artist_name`, `record_label_id`) VALUES
(1, 'Oliver Tree', 1),
(2, 'Joji', 2);

-- --------------------------------------------------------

--
-- Table structure for table `record_label`
--

CREATE TABLE `record_label` (
  `record_label_id` int NOT NULL,
  `record_label_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `record_label`
--

INSERT INTO `record_label` (`record_label_id`, `record_label_name`) VALUES
(1, 'Atlantic Records'),
(2, '88Rising');

-- --------------------------------------------------------

--
-- Table structure for table `song`
--

CREATE TABLE `song` (
  `song_id` int NOT NULL,
  `song_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `song_length` time NOT NULL,
  `album_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `song`
--

INSERT INTO `song` (`song_id`, `song_name`, `song_length`, `album_id`) VALUES
(1, 'ATTENTION', '00:02:03', 2),
(2, 'SLOW DANCING IN THE DARK', '00:03:29', 2),
(3, 'TEST DRIVE', '00:02:59', 2),
(4, 'WANTED U', '00:04:11', 2),
(5, 'CAN\'T GET OVER YOU (feat. Clams Casino)', '00:01:47', 2),
(6, 'YEAH RIGHT', '00:02:54', 2),
(7, 'WHY AM I STILL IN LA(feat. Schlohmo & D33J)', '00:03:19', 2),
(8, 'NO FUN', '00:02:48', 2),
(9, 'COME THRU', '00:02:33', 2),
(10, 'R.I.P.(feat. Trippie Redd)', '00:02:38', 2),
(11, 'XNXX', '00:02:07', 2),
(12, 'I\'LL SEE YOU IN 40', '00:04:13', 2),
(13, 'Me, Myself & I', '00:02:53', 1),
(14, '1993(feat. Little Ricky ZR3', '00:02:39', 1),
(15, 'Cash Machine', '00:02:56', 1),
(16, 'Let Me Down', '00:01:51', 1),
(17, 'Miracle Man', '00:02:05', 1),
(18, 'Bury Me Alive', '00:02:44', 1),
(19, 'Alien Boy', '00:02:44', 1),
(20, 'Joke\'s On You!', '00:03:11', 1),
(21, 'Again & Again', '00:02:54', 1),
(22, 'Waste My TIme', '00:03:27', 1),
(23, 'Jerk', '00:02:15', 1),
(24, 'Hurt', '00:02:25', 1),
(25, 'Introspective', '00:02:16', 1),
(26, 'I\'m Gone', '00:03:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int NOT NULL,
  `user_first_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_last_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_salt` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` int NOT NULL DEFAULT '1',
  `user_active` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first_name`, `user_last_name`, `user_email`, `user_password`, `user_salt`, `user_role`, `user_active`) VALUES
(1, 'John', 'Doe', 'john@doe.com', 'opensesame', 'xxx', 1, 'yes'),
(2, 'Jane', 'Doe', 'jane@doe.com', 'letmein', 'xxx', 2, 'no'),
(3, 'Bob', 'Smith', 'bob@smith.com', 'test', 'xxx', 2, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_role_id` int NOT NULL,
  `user_role_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role_desc` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_role_id`, `user_role_name`, `user_role_desc`) VALUES
(1, 'Standard User', 'Normal user with no special permissions'),
(2, 'Admin', 'Extra permissions');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `album`
--
ALTER TABLE `album`
  ADD PRIMARY KEY (`album_id`),
  ADD KEY `artist_id` (`artist_id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
  ADD PRIMARY KEY (`artist_id`),
  ADD KEY `record_label_id` (`record_label_id`);

--
-- Indexes for table `record_label`
--
ALTER TABLE `record_label`
  ADD PRIMARY KEY (`record_label_id`);

--
-- Indexes for table `song`
--
ALTER TABLE `song`
  ADD PRIMARY KEY (`song_id`),
  ADD KEY `album_id` (`album_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`),
  ADD KEY `user_role` (`user_role`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `album`
--
ALTER TABLE `album`
  MODIFY `album_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
  MODIFY `artist_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `record_label`
--
ALTER TABLE `record_label`
  MODIFY `record_label_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `song`
--
ALTER TABLE `song`
  MODIFY `song_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `user_role_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `album_ibfk_1` FOREIGN KEY (`artist_id`) REFERENCES `artist` (`artist_id`);

--
-- Constraints for table `artist`
--
ALTER TABLE `artist`
  ADD CONSTRAINT `artist_ibfk_1` FOREIGN KEY (`record_label_id`) REFERENCES `record_label` (`record_label_id`);

--
-- Constraints for table `song`
--
ALTER TABLE `song`
  ADD CONSTRAINT `song_ibfk_1` FOREIGN KEY (`album_id`) REFERENCES `album` (`album_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_role`) REFERENCES `user_roles` (`user_role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
